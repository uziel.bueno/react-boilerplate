import React from 'react';

const LoadingPage = (props) => (
  <div className="loader">
    <img className="loader__image" src="/img/loader.gif" />
  </div>
);

export default LoadingPage;
