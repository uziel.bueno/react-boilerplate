import authReducer from '@/reducers/auth';
import { login, logout } from '@/actions/auth';

test('should set uid for login', () => {
  const uid = '123abc';

  const state = authReducer({}, login(uid));

  expect(state.uid).toBe(uid);
});

test('should clear uid for logout', () => {
  const uid = '123abc';

  const state = authReducer({ uid }, logout());

  expect(state).toEqual({});
});
