import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import HelpPage from '@components/HelpPage';
import PrivateRoute from './PrivateRoute';
import LoginPage from '@/components/LoginPage';
import NotFoundPage from '@components/NotFoundPage';
import DashboardPage from '@/components/DashboardPage';
import PublicRoute from './PublicRoute';

export const history = createBrowserHistory();

const AppRouter = (props) => (
  <Router history={history}>
    <div>
      <Switch>
        <PublicRoute path="/" component={LoginPage} exact={true} />
        <PrivateRoute path="/dashboard" component={DashboardPage} />
        <Route path="/help" component={HelpPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
);

export default AppRouter;
